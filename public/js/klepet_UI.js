/** KONCNA VERZIJA 2
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
var originalnoIme = [];
var nadImki = [];

function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}

function preimenovalnaTabela(uporabnik, nadimek){
  var stop = 0;
  for(var i = 0; i<originalnoIme.length;i++){
    if(originalnoIme[i] == uporabnik){
      nadImki[i] = nadimek;
      stop = 1;
    }
  }
  if(stop == 0){
    originalnoIme.push(uporabnik);
   nadImki.push(nadimek);
  }
}
/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  var jeKrc = sporocilo.indexOf("&#9756;") > -1;
  var zacasno2 = sporocilo.split(" ");
  var jeSlika = 0;
  for(var i = 0; i<zacasno2.length;i++){
    var zacasno = preveriZaSliko(zacasno2[i]);
    for(var j = 0;j<zacasno.length;j++){
      var jeSmesko2 = zacasno[j].indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
    if(zacasno[j] != 0 && !(jeSmesko2)){
      sporocilo += '<br>'+'<img src='+"\""+zacasno[j]+"\"" + "style=\"width:200px;padding-left: 20px\"" +'>';
      jeSlika++;
    }
    }
  }
  zacasno2 = zacasno2.join(' ');

  if (jeSmesko || jeSlika != 0 || jeKrc) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("20px\"&gt;").join("20px\">")
               .split("&lt;br&gt;").join("<br>")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}

function preveriZaSliko(kandidat) {
   var regularenIzraz1 = new RegExp('\\b' + "https" + '.*?' + ".jpg" + '\\b', 'gi');
   var regularenIzraz2 = new RegExp('\\b' + "https" + '.*?' + ".png" + '\\b', 'gi');
   var regularenIzraz3 = new RegExp('\\b' + "https" + '.*?' + ".gif" + '\\b', 'gi');
   var regularenIzraz4 = new RegExp('\\b' + "http" + '.*?' + ".jpg" + '\\b', 'gi');
   var regularenIzraz5 = new RegExp('\\b' + "http" + '.*?' + ".png" + '\\b', 'gi');
   var regularenIzraz6 = new RegExp('\\b' + "http" + '.*?' + ".gif" + '\\b', 'gi');
   var match;
   if(regularenIzraz1.test(kandidat)){
     match = kandidat.match(regularenIzraz1);
     return match;
   }
   else if(regularenIzraz2.test(kandidat)){
     match = kandidat.match(regularenIzraz2);
     return match;
   }
    else if(regularenIzraz3.test(kandidat)){
    match = kandidat.match(regularenIzraz3);
     return match;
   }
    else if(regularenIzraz4.test(kandidat)){
     match = kandidat.match(regularenIzraz4);
     return match;
   }
    else if(regularenIzraz5.test(kandidat)){
     match = kandidat.match(regularenIzraz5);
     return match;
   }
    else if(regularenIzraz6.test(kandidat)){
     match = kandidat.match(regularenIzraz6);
     return match;
   }
 return 0;
}

var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var zacasno = sporocilo.besedilo;
    zacasno = zacasno.split(' ');
    var kandidatNadimka;
    if(zacasno[0] == 'posebnoSporociloPreimenovanjeUporabnika'){
      var zacetek;
      kandidatNadimka ='';
      var original = '';
      for(var i = 1;i<zacasno.length;i++){
        if(zacasno[i] ==  'prelomPosebnegaSporocila'){
          zacetek = i+1;
          break;
        }
        if(zacasno[i+1] == 'prelomPosebnegaSporocila'){
           original += zacasno[i];
        }
       else{
         original += zacasno[i] +' ';
       }
      }
      for(var i = zacetek; i<zacasno.length;i++){
        if(i != zacasno.length-1){
          kandidatNadimka += zacasno[i] +' ';
        }
        else{
          kandidatNadimka += zacasno[i];
        }
      }
      for(var i = 0;i<nadImki.length;i++){
      if(original == originalnoIme[i]){
        originalnoIme[i] = kandidatNadimka;
      }
    }
    }
    else{
       var prvi = 1;
    for(var i = 0;i<zacasno.length;i++){
      if(zacasno[i] == 'se' && zacasno.length-i > 3){
        prvi = i;
      }
    }
    if(zacasno.length > 5&&(zacasno[prvi] == 'se' && zacasno[prvi+1] == 'je' && zacasno[prvi+2] == 'preimenoval' && zacasno[prvi+3] == 'v')){
      kandidatNadimka="";
      for(var i = prvi+4;i<zacasno.length;i++){
        if(i == zacasno.length-1){
          kandidatNadimka += zacasno[i];
        }
        else{
          kandidatNadimka += zacasno[i] + ' ';
        }
      }
      kandidatNadimka = kandidatNadimka.slice(0,kandidatNadimka.length-1);
    }
     zacasno = zacasno.join(' ');
    for(var i = 0;i<nadImki.length;i++){
      if(zacasno == originalnoIme[i]+ " se je preimenoval v " + kandidatNadimka + "."){
        originalnoIme[i] = kandidatNadimka;
      }
    }
    zacasno = zacasno.split(':');
    for(var i = 0;i<nadImki.length;i++){
      if(originalnoIme[i] == zacasno[0]){
        zacasno[0] = nadImki[i] + ' ('+zacasno[0] + ')';
      }
      if(originalnoIme[i] + ' (zasebno)' == zacasno[0]){
        var zacasno2 = zacasno[0];
         zacasno2 = zacasno2.split(' ');
        zacasno2.splice(zacasno2.length-1,1);
        zacasno2 = zacasno2.join(' ');
        zacasno[0] = nadImki[i] +' ('+ zacasno2 + ') (zasebno)';
      }
    }
    var zacasno = zacasno.join(':');
    sporocilo.besedilo = zacasno;
    var novElement = divElementEnostavniTekst(sporocilo.besedilo);
    $('#sporocila').append(novElement);
    }
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    for (var i=0; i < uporabniki.length; i++) {
      var imaNadimek = 0;
      for(var j = 0;j<nadImki.length;j++){
        if(originalnoIme[j] == uporabniki[i]){
           $('#seznam-uporabnikov').append(divElementEnostavniTekst(nadImki[j] + " ("+uporabniki[i]+")"));
           imaNadimek++;
        }
      }
      if(imaNadimek==0){
         $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
      }
    }
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-uporabnikov div').click(function() {
      var prejemnik = $(this).text();
      var prejemnikFinal = [];
      var aliNadimek = 0;
      for(var i = 0; i<prejemnik.length;i++){
        if(prejemnik[i]=='('){
          for(var j = i+1;j<prejemnik.length;j++){
            if(prejemnik[j]==')'){
              var stevec = 0;
              for(var k = i+1;k<j;k++){
                prejemnikFinal[stevec]=prejemnik[k];
                stevec++;
              }
              aliNadimek = 1;
              break;
            }
          }
          break;
        }
      }
      if(aliNadimek == 1){
        prejemnikFinal = prejemnikFinal.join('');
      }
      else{
        prejemnikFinal = prejemnik;
      }
     var sporocilo = klepetApp.procesirajUkaz('/zasebno ' + '"' + prejemnikFinal + '" ' + '"&#9756;"');
       $('#sporocila').append(divElementHtmlTekst(sporocilo));
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
